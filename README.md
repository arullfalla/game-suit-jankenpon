Oke langsung saja kita buat permainan ini mengunakan javascript.
Disini kita akan menggunakan pop-up prompt untuk user memasukkan apa yang akan pilih. Apakah itu batu, kertas, atau gunting.

Jadi konsepnya user memasukan inputan melalui prompt setelah itu komputer akan secara random memilih juga antara batu, kertas, gunting. Dimana setelah itu akan muncul notifikasi kalau user tadi menang, kalah, ataupun seri.

Pertama kita akan buat dulu fungsi HTML nya. Dimana fungsi ini akan memanggil file javascript. Disini kita akan beri nama index.html . Dimana file ini yang akan di panggil pertama kali ketika kita menjalankan browser.